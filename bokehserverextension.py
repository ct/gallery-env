from subprocess import Popen
from os import chdir

def load_jupyter_server_extension(nbapp):
    chdir("examples/bacteria")
    Popen(["bokeh", "serve", "bacteria-panel.ipynb", "--allow-websocket-origin=*", "--port=5006"])
    chdir("../..")
    chdir("examples/swimmer")
    Popen(["bokeh", "serve", "swimmer.ipynb", "--allow-websocket-origin=*", "--port=5007"])
    chdir("../..")
